from django.db import models


class Book(models.Model):
    # Required Fields
    title = models.CharField(max_length=512)
    author = models.ForeignKey('Author', on_delete=models.CASCADE)
    edition = models.CharField(max_length=128)
    year = models.IntegerField()
    publisher = models.CharField(max_length=512)
    place = models.CharField(max_length=128)

    # Optional Fields
    additional_authors = models.ManyToManyField(
        'Author', blank=True, related_name='additional_authors_set')
    isbn = models.IntegerField(blank=True, null=True)
    doi = models.CharField(max_length=128, blank=True, null=True)

    def __str__(self):
        return(f"{self.title} ({self.author.surname})")


class Author(models.Model):
    surname = models.CharField(max_length=128)
    initials = models.CharField(max_length=64)
    firstnames = models.CharField(max_length=256)

    def __str__(self):
        return(f"{self.firstnames} {self.surname}")

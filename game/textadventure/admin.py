from django.contrib import admin
from .models import Station, Choice, Item, Player, RelatedChoice, Event

admin.site.register(Station)
admin.site.register(Choice)
admin.site.register(Item)
admin.site.register(Player)
admin.site.register(Event)
admin.site.register(RelatedChoice)

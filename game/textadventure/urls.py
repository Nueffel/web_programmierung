from django.urls import path
from . import views

urlpatterns = [
        path('', views.index, name='index'),
        path('start_game', views.start_game, name='start_game'),
        path('game/<player>/<station>', views.station, name='station'),
        path('game/<player>/<station>/<event>/<choice>',
             views.choice, name='choice'),
        path('end/<player>', views.end, name='end'),
        ]

# Generated by Django 4.0.3 on 2022-06-05 13:37

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('textadventure', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='required_event',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='textadventure.event'),
        ),
    ]

from django.shortcuts import get_object_or_404, render, redirect
from .models import Choice, Player, Station, Event
from .escape import get_hearts, get_available_choices, get_current_events

DEFAULT_ACTIONS = 10


def index(request):
    return render(request, 'textadventure/index.html')


def start_game(request):
    player_name = request.POST['player_name']

    # dissallow empty names
    if not player_name:
        return redirect(index)

    # try to use existing player
    try:
        player = Player.objects.get(name=player_name)
    except Player.DoesNotExist:
        start_station = Station.objects.get(id_name='start')
        player = Player.objects.create(
                name=player_name, start_actions=DEFAULT_ACTIONS,
                actions=DEFAULT_ACTIONS, current_station=start_station)

    return redirect(station, station='start', player=player)


def station(request, station, player):
    player = get_object_or_404(Player, name=player)
    station = get_object_or_404(Station, id_name=station)

    # if the station is visited the first time add it to the discovered
    # stations and start the story
    if station not in player.discovered_stations.all():
        player.discovered_stations.add(station)
        # add the entry text instead of writing to the story because when you
        # enter the first time there is also the results text of the last event
        player.last_action = f"{player.last_action} {station.first_enter_text}"
    else:
        # Only add message if the station has changed
        if station != player.current_station:
            player.story_text = f"{player.story_text} {player.last_action}"
            player.last_action = station.enter_text

    player.current_station = station
    player.save()

    # End game if there are no turns left
    if player.actions < 0 or station.id_name == 'end':
        return redirect(end, player)

    # returns "no_event" if there is none currently
    current_events = get_current_events(player, station)
    if current_events == "no_event":
        current_events = [{'event_text': 'Nothing to do here'}, ]
        choices = []
    else:
        choices = get_available_choices(player, current_events)

    items = player.items.all()

    # Avoid a long backlog
    story = player.story_text
    if len(player.story_text) > 200:
        story = f"...{story[-200:]}"

    context = {
            'player': player,
            'items': items,
            'station': station,
            'events': current_events,
            'choices': choices,
            'story': story,
            'hearts': get_hearts(player),
            'discovered_stations': player.discovered_stations.all()
            }
    return render(request, 'textadventure/game.html', context)


def choice(request, player, station, event, choice):
    choice = get_object_or_404(Choice, id=choice)
    player = get_object_or_404(Player, name=player)
    event = get_object_or_404(Event, id_name=event)
    print(f"CHOICE: {choice} for EVENT: {event}")

    if choice.recieve_item:
        player.items.add(choice.recieve_item)

    player.chosen_choices.add(choice)
    player.actions -= 1

    # if the choice ahd a related choice add it to the discarded choices
    for discarded_choice in choice.related_choices.all():
        player.discarded_choices.add(discarded_choice)
    # Check whether the event is finished (no choices left)
    if not event.require_all_options:
        player.finished_events.add(event)
    else:
        all_event_choices = Choice.objects.filter(event=event)
        all_done = True
        for c in all_event_choices:
            if (c not in player.discarded_choices.all() and
                    c not in player.chosen_choices.all()):
                all_done = False
        if all_done:
            player.finished_events.add(event)

    # Insert last action into the story text and the result into last action
    player.story_text = f"{player.story_text} {player.last_action}"

    # adds the event text to the story before the first selected choice
    if player.story_text.find(event.event_text):
        player.story_text = f"{player.story_text} {event.event_text}"
    player.last_action = choice.action_result

    player.save()

    return redirect(
            'station', player=player, station=choice.next_station.id_name,
            )


def end(request, player):
    player = get_object_or_404(Player, name=player)
    if player.actions >= 0:
        good_end = True
    else:
        good_end = False
    context = {
            'player': player,
            'good_end': good_end,
            }
    return render(request, 'textadventure/end.html', context)

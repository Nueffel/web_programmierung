from django.db import models


class Station(models.Model):
    id_name = models.CharField(max_length=32, unique=True)
    name = models.CharField(max_length=512)
    first_enter_text = models.TextField()
    enter_text = models.TextField()

    def __str__(self):
        return f"{self.name} ({self.id_name})"


class Event(models.Model):
    id_name = models.CharField(max_length=32, unique=True)
    event_text = models.TextField()
    station = models.ForeignKey(to=Station, on_delete=models.PROTECT)

    # if all options need to be used before moving on
    require_all_options = models.BooleanField(default=True)
    required_event = models.ForeignKey(
            to='Event', on_delete=models.PROTECT, blank=True, null=True,
            related_name="required_event_set")

    def __str__(self):
        return f"{self.station.name} - {self.id_name}"

    # Ordering for better editing on the admin page
    class Meta:
        ordering = ["station", "id_name"]


class Choice(models.Model):
    action_text = models.CharField(max_length=32)
    action_result = models.TextField()
    event = models.ForeignKey(to=Event, on_delete=models.PROTECT)

    related_choices = models.ManyToManyField(
            'self', through='RelatedChoice', symmetrical=False)
    required_item = models.ForeignKey(
            to='Item', blank=True, null=True, on_delete=models.SET_NULL,
            related_name='required_item_set')
    next_station = models.ForeignKey(
            to=Station, on_delete=models.PROTECT,
            related_name="next_station_set")
    recieve_item = models.ForeignKey(
            to='Item', blank=True, null=True, on_delete=models.SET_NULL)

    def __str__(self):
        return f"{self.event} - {self.action_text}"

    # Ordering for better editing on the admin page
    class Meta:
        ordering = ["event", "action_text"]


# Used to allow for the related_choices in choice to delete one
# if the other is selected
class RelatedChoice(models.Model):
    from_choice = models.ForeignKey(
            Choice, related_name="from_choice", on_delete=models.PROTECT)
    to_choice = models.ForeignKey(
            Choice, related_name="to_choice", on_delete=models.PROTECT)

    def __str__(self):
        return f"FROM '{self.from_choice}' TO '{self.to_choice}'"


class Item(models.Model):
    name = models.CharField(max_length=512)
    description = models.TextField()

    def __str__(self):
        return self.name


class Player(models.Model):
    name = models.CharField(unique=True, max_length=64)
    start_actions = models.IntegerField(default=10)
    actions = models.IntegerField()
    current_station = models.ForeignKey(
            to=Station, on_delete=models.PROTECT,
            related_name="current_station_set")
    items = models.ManyToManyField(Item)

    finished_events = models.ManyToManyField(Event)
    chosen_choices = models.ManyToManyField(
            Choice, related_name="chosen_choices_set")
    discarded_choices = models.ManyToManyField(
            Choice, related_name="discarded_choices_set")
    discovered_stations = models.ManyToManyField(Station)

    last_action = models.TextField(default="")
    story_text = models.TextField(default="")

    def __str__(self):
        return self.name

from django.http.response import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.template import loader
from django.urls import reverse

from .models import todo_entry

def index(request):
    latest_todos = todo_entry.objects.order_by('todo_date')
    context = {'latest_todos': latest_todos,}
    return render(request, 'todo/index.html', context)

def new_entry(request):
    todo_entry.objects.create(todo_text=request.POST["new_todo_text"],
                              todo_date=request.POST["new_todo_date"],
                              todo_progress=False)

    return HttpResponseRedirect(reverse('todo:index'))

def delete_entry(request):
    entry = get_object_or_404(todo_entry, id=request.POST["id"])
    entry.delete()
    return HttpResponseRedirect(reverse('todo:index'))

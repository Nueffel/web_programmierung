from django.db import models
from django.utils import timezone

class todo_entry(models.Model):

    todo_text = models.CharField(max_length=200)
    todo_progress = models.BooleanField(default=False)
    todo_date = models.DateField(default=timezone.now)

    def __str__(self):
        return f"{self.todo_text}"

from django.shortcuts import render
from django.shortcuts import get_object_or_404, render
from django.http.response import HttpResponseRedirect
from django.urls import reverse
from . import models

# Create your views here.
def index(request):
    all_lists = models.shopping_list.objects.order_by('shopping_list_name')
    all_shops = models.shop_entry.objects.order_by('shop_name')
    context = { 'all_lists' : all_lists,
                'all_shops' : all_shops, }
    return render(request, 'shopping_list/index.html' , context)

def list(request, list_id):
    all_list_entries = models.shopping_list_entry.objects.filter(shopping_list_entry_list=list_id)
    print(list_id)
    print(all_list_entries)
    all_list_entries.order_by('shopping_list_entry_name')
    context = { 'all_list_entries': all_list_entries,}
    return render(request, 'shopping_list/list.html', context)

def shop(request, shop_id):
    all_shop_entries = models.shopping_list_entry.objects.filter(shopping_list_entry_shop=shop_id)
    context = { 'all_shop_entries': all_shop_entries,}
    return render(request, 'shopping_list/shop.html')

def new_list_entry(request):
    models.shopping_list.objects.create(shopping_list_entry_name=request.POST["new_shopping_list_entry_name"],
                                        shopping_list_entry_amount=request.POST["new_shopping_list_entry_amount"],
                                        shopping_list_entry_price=request.POST["new_shopping_list_entry_price"],
                                        shopping_list_entry_shop=request.POST["new_shopping_list_entry_shop"])
    return HttpResponseRedirect(reverse('shopping_list:index'))

def new_list(request):
    models.shopping_list.objects.create(shopping_list_name=request.POST["new_shopping_list_name"])
    return HttpResponseRedirect(reverse('shopping_list:index'))

def delete_list(request):
    list = get_object_or_404(models.shopping_list, id=request.POST["id"])
    list.delete()
    return HttpResponseRedirect(reverse('shopping_list:index'))

def new_shop(request):
    models.shop_entry.objects.create(shop_name=request.POST["new_shop_name"],
                                     shop_open_time=request.POST["new_shop_open_time"])
    return HttpResponseRedirect(reverse('shopping_list:index'))

def delete_shop(request):
    shop = get_object_or_404(models.shop_entry, id=request.POST["id"])
    shop.delete()
    return HttpResponseRedirect(reverse('shopping_list:index'))

def delete_shopping_list_entry(request):
    shopping_list_entry = get_object_or_404(models.shopping_list_entry, id=request.POST["id"])
    shopping_list_entry.delete()
    return HttpResponseRedirect(reverse('shopping_list:index'))

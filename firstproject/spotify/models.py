from django.db import models

class Track(models.Model):
    name = models.CharField(max_length=512)
    spotify_id = models.CharField(max_length=32)
    length = models.IntegerField()
    
    def __str__(self):
        return self.name

class RecentTrack(models.Model):
    track = models.ForeignKey(Track, on_delete=models.CASCADE)
    played_at = models.DateTimeField()
    
    def __str__(self):
        return f"{self.played_at} {self.track.name}"

class Artist(models.Model):
    name = models.CharField(max_length=512)
    spotify_id = models.CharField(max_length=32)
    tracks = models.ManyToManyField(Track)
    
    def __str__(self):
        return self.name

class Genre(models.Model):
    name = models.CharField(max_length=64)
    artists = models.ManyToManyField(Artist)
    
    def __str__(self):
        return self.name

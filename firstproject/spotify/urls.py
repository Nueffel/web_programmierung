from django.urls import path
from . import views

urlpatterns = [
        path('', views.index, name='index'),
        path('spotify_callback', views.spotify_callback, name='spotify_callback'),
        path('change_account', views.change_account, name='change_account'),
        path('track/<id>', views.track, name='track'),
        path('artist/<id>', views.artist, name='artist'),
        path('genre/<id>', views.genre, name='genre'),
        ]


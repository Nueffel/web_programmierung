from datetime import datetime

def millis_to_string(millis):
    seconds=(millis/1000)%60
    minutes=(millis/(1000*60))%60
    return f"{minutes:0>2.0f}:{seconds:0>2.0f}"


def print_log(title, text):
    BOLD='\033[1m'
    NORMAL ='\033[0m'
    ts = datetime.now().strftime("%H:%M:%S")
    print(f"\n{BOLD}{ts} {title.upper()}:{NORMAL} {text}")

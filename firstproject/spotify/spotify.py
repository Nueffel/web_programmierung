from . import cred
from .cache_handler import CacheHandler
from .models import Track, Artist, Genre, RecentTrack  
from django.shortcuts import redirect 
from urllib.parse import urlencode
from datetime import datetime, timedelta
import base64, requests, json
from .utilities import print_log


AUTH_URL = "https://accounts.spotify.com/authorize"
TOKEN_URL = "https://accounts.spotify.com/api/token"
CLIENT_ID = cred.client_id
CLIENT_SECRET = cred.client_secret

class SpotifyClient:
    def __init__(self):
        self.client_id = CLIENT_ID
        self.client_secret = CLIENT_SECRET
        self.redirect_uri = 'http://localhost:8000/spotify/spotify_callback'
        self.cache_handler = CacheHandler()
        self.access_token, self.refresh_token, self.token_expires_at = self.cache_handler.get_cache_token("", "", datetime.now())
        self.spotify_authorization_code = ""  # code from spotify, auth was successful if its not empty
        self.scope = 'user-read-recently-played'

    # forget userdata
    def logout(self):
        self.access_token = ""
        self.spotify_authorization_code = ""
        self.refresh_token = ""
        self.cache_handler.delete_cache()


    # if there are there are refresh and access token, we're logged in
    def is_logged_in(self):
        return self.access_token and self.refresh_token


    def get_connect_redirect(self):
        params = urlencode({
           'client_id' : self.client_id,
           'scope' : self.scope,
           'redirect_uri' : self.redirect_uri,
           'response_type' : 'code',
           'show_dialog' : True,
           })
        url = f"{AUTH_URL}?{params}"
        print_log("URL", url)
        return redirect(url)


    def get_token(self):
        # Check if there is a valid token already
        if self.access_token and datetime.now() < self.token_expires_at:
            return self.access_token
        
        if self.refresh_token:
            url_data = {
                'grant_type': 'refresh_token',
                'refresh_token': self.refresh_token,
                'redirect_uri': self.redirect_uri,
                }
        else:
            url_data = {
                'grant_type': 'authorization_code',
                'code': self.spotify_authorization_code,
                'redirect_uri': self.redirect_uri,
                }

        url_headers = {
            'Authorization': 'Basic ' +
            base64.b64encode(f"{self.client_id}:{self.client_secret}".encode("UTF8")).decode("UTF8"),
            }
        print(url_data)
        response = requests.post(TOKEN_URL, data=url_data, headers=url_headers)
        print_log("RESPONSE", response.text)

        data = json.loads(response.text)
        self.access_token = data['access_token']
        if 'refresh_token' in data:
            self.refresh_token = data['refresh_token'] 
        self.token_expires_at = datetime.now() + timedelta(seconds=data['expires_in'])
        self.cache_handler.set_cache_token(data, self.token_expires_at, self.refresh_token)
        return self.access_token

    
    def get_spotify_data(self, access_url, params={}):
        base_url = 'https://api.spotify.com/v1'
        url_headers = {
            'Authorization': 'Bearer ' + self.get_token(),
            'Content-Type': 'application/json',
            }
        response = requests.get(base_url + access_url, headers=url_headers, params=params)
        print_log(f"RESPONSE ({access_url})", response)
        data = json.loads(response.text)
        return data


    def update_recently_played(self):
        access_url = '/me/player/recently-played'
        params = {'limit': 50}
        data = self.get_spotify_data(access_url, params)

        self.recently_played_into_database(data)
        return data
 

    def recently_played_into_database(self, data):
        
        for t in data['items']:
            track_name = t['track']['name']
            track_id = t['track']['id']
            track_length = t['track']['duration_ms']
            
            # Get track or create a new one
            # track = Track.objects.get_or_create(spotify_id=track_id, name=track_name, length=track_length)
            try:
                track = Track.objects.get(spotify_id=track_id)
            except Track.DoesNotExist:
                track = Track.objects.create(spotify_id=track_id,
                                             name=track_name,
                                             length=track_length)
            # Add recent track
            try:
                recent_track = RecentTrack.objects.get(played_at=t['played_at'], track=track)
            except RecentTrack.DoesNotExist:
                recent_track = RecentTrack.objects.create(track=track, played_at=t['played_at'])
                print_log("Added", recent_track)

            # Get artists or create a new one
            for a in t['track']['album']['artists']:
                artist_id = a['id']
                artist_name = a['name']

                try:
                    artist = Artist.objects.get(spotify_id=artist_id)
                except Artist.DoesNotExist:
                    artist = Artist.objects.create(spotify_id=artist_id,
                                             name=artist_name)
                                                                 
                    self.get_artist_genre(artist) 
                
                artist.tracks.add(track)


    def get_artist_genre(self, artist):
        access_url = '/artists/' + artist.spotify_id
        data = self.get_spotify_data(access_url)

        genres = data['genres']

        for g in genres:
            try: 
                artist_genre = Genre.objects.get(name=g)
            except Genre.DoesNotExist:
                artist_genre = Genre.objects.create(name=g)

            artist_genre.artists.add(artist)

        return data

from .models import Track, Artist, Genre, RecentTrack
from .utilities import millis_to_string, print_log

def get_genre_info(genre):

    genre_artists = genre.artists.all()
    genre_tracks = []
    for artist in genre_artists:
        new_genre_tracks = artist.tracks.all()
        for track in new_genre_tracks:
            genre_tracks.append(track)

    recently_played_all = RecentTrack.objects.all().order_by('-played_at')

    recently_played = []
    for play in recently_played_all:
        if play.track in genre_tracks:
            recently_played.append({
                    'track' : play.track,
                    'length' : millis_to_string(play.track.length),
                    'played_at': play.played_at.strftime("%H:%M %d.%m.%Y"),
                    })

    return {'recently_played': recently_played, 'genre_tracks': genre_tracks, 'genre_artists': genre_artists}


def get_artist_info(spc, artist):
    artist_data = spc.get_spotify_data(f"/artists/{artist.spotify_id}")
    image_url = artist_data['images'][0]['url']
    open_in_spotify_url = artist_data['external_urls']['spotify']

    artist_tracks = artist.tracks.all()
    recently_played_all = RecentTrack.objects.all().order_by('-played_at')
    
    recently_played = []
    for pl in recently_played_all:
        if pl.track in artist_tracks:
            recently_played.append(pl) 
    
    genres = get_genre(artist)

    return { 'genres': genres,
             'recently_played' : recently_played,
             'artist_tracks': artist_tracks,
             'image_url' : image_url, 
             'open_in_spotify_url': open_in_spotify_url}


def get_track_info(spc, track):
    track_data = spc.get_spotify_data(f"/tracks/{track.spotify_id}")
    
    cover_url = track_data['album']['images'][0]['url']
    open_in_spotify_url = track_data['external_urls']['spotify']
    album = track_data['album']['name']
    genres = get_genre(track)

    recently_played = RecentTrack.objects.filter(track=track).order_by('-played_at')
    artists = Artist.objects.filter(tracks = track)

    return {'album': album,
            'artists': artists,
            'recently_played': recently_played,
            'genres' : genres,
            'cover_url': cover_url, 
            'open_in_spotify_url': open_in_spotify_url}

def get_genre(input):
    
    if isinstance(input, Track):
        artists = Artist.objects.filter(tracks = input)
    elif isinstance(input, Artist):
        artists = [input,]
    else: 
        print("INVALID GENRE INPUT")
        return

    genres = []
    for artist in artists:
        artist_genres = Genre.objects.filter(artists = artist)
        for g in artist_genres:
            if g not in genres:
                genres.append(g)

    return genres



def get_recently_played_list(item_count):
    recently_played = RecentTrack.objects.order_by('-played_at')[:item_count]
    
    recently_played_list = []
    for play in recently_played:
        artists = Artist.objects.filter(tracks__in=[play.track])
        recently_played_list.append({
                'track' : play.track,
                'length' : millis_to_string(play.track.length),
                'played_at': play.played_at.strftime("%H:%M %d.%m.%Y"),
                'artists': artists, 
                })

    return recently_played_list


def get_weighted_sorted_lists():
    recent_tracks = RecentTrack.objects.all()
    
    weighted_tracks = {}
    weighted_artists = {}
    weighted_genres = {}

    for recent_track in recent_tracks:
        track = recent_track.track

        time_weight = 1 # weight based on how long ago a track was played (currently not used)
        split_weight = 1 # weight based on how many artists / tracks are assosiated with a track
            
        # Add Track
        track_weight = time_weight
        if track in weighted_tracks:
            track_weight += weighted_tracks[track]
        weighted_tracks.update({track : track_weight})
        
        artists = Artist.objects.filter(tracks = track)
        if artists: # check if the track has an assigned artist
            split_weight /= len(artists) # less weight if there are more artists

        for artist in artists:
                            
            # Add Artists
            artist_weight = time_weight
            if artist in weighted_artists:
                artist_weight += weighted_artists[artist]
            weighted_artists.update({artist : artist_weight})

            genres = Genre.objects.filter(artists = artist)
            if genres: # check if the artist has assigned genres
                split_weight /= len(genres)
            for genre in genres: 
                genre_weight = time_weight * split_weight

                if genre in weighted_genres:
                    genre_weight += weighted_genres[genre]

                weighted_genres.update({genre : genre_weight})

    for key, weight in weighted_genres.items(): # Prettier output but a bit less accurate
        weighted_genres[key] = round(weight * 10)

    sorted_tracks = sorted(weighted_tracks.items(),key= lambda x:x[1], reverse=True)
    sorted_artists = sorted(weighted_artists.items(),key= lambda x:x[1], reverse=True)
    sorted_genres = sorted(weighted_genres.items(),key= lambda x:x[1], reverse=True)

    return sorted_tracks, sorted_artists, sorted_genres 
